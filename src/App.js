import React from 'react'
import './App.css'

import Calculadora from './componentes/calculadora/Calculadora'
import Caderno from './componentes/Caderno/Caderno'
import Lampada from './componentes/Lampada/Lampada'


function App(){
  return(
    <div>
    <main className = 'container'>
    Maria's Table Essentials
      <Calculadora />
  
    </main>
    <div className = 'container2'>
      <Caderno />
    </div>
    <div className = 'container3'>
      <Lampada />
    </div>

   </div>
  )
}

export default App