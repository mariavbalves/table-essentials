import React, { useState } from 'react'
import './Lamapada.css'


function Lampada(){
return(
    <div className = "lamp">
          <div className = "shade"></div>
          <div className = "leg"></div>
          <div className = "foot"></div>
    </div>
  )

}
    export default Lampada